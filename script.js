let section = document.querySelector(".notas");
let result = document.querySelector(".result");
let btr_add = document.querySelector(".add");
let btr_calc = document.querySelector(".calc");
var cont = 0;
var list = [];

btr_add.addEventListener("click", () => {
    let input = document.querySelector(".nota");
    if(input.value.trim() == ""){
        return alert("Por favor, insira uma nota");
    } else if(input.value < 0 || input.value > 10){
        return alert("A nota é invalida. Por favor insira uma nota valida")
    }
    let data = document.createElement("p");
    cont++;
    data.innerText = "A nota " + cont + " foi " + input.value;
    list.push(input.value);
    section.append(data);
});

btr_calc.addEventListener("click",() => {
    let sum = 0;
    for(let i = 0; i < cont; i++){
        sum = sum + parseFloat(list[i]);
    }
    media = sum/list.length;
    result.innerText = "A média é: " + media;
    section.innerHTML = "";
    cont = 0;
    list.length = 0;
});
